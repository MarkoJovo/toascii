#include <opencv2/opencv.hpp>
#include <opencv2/core/types.hpp>
#include <iostream>
#include <fstream>
#include <vector>
using namespace cv;


//Usage: command line argument is the image youd like to read. chars are assumed to be twice as tall as they are wide, so vertical scale factor is doubled.
//uses openCV for pixel data

int main( int argc, char** argv )
{
	int scale;
	std::cout<<"Enter Scale Factor ( Ascii : Image) 1 : ";
	std::cin>>scale;
 char* imageName = argv[1];
 Mat image;
 image = imread( imageName, IMREAD_GRAYSCALE );
 std::ofstream output;
 output.open("op.txt");

 if( argc != 2 || !image.data )
 {
   printf( " No image data \n " );
   return -1;
 }

int verticalScale = 2;
int horizChars = image.cols / scale;
int vertiChars = image.rows / (scale*verticalScale);

std::cout<<horizChars<<std::endl;	
std::cout<<vertiChars<<std::endl;	
char asciiList[10] = {' ','.',',',';','^','*','/','$','%','@'};
int sum;
int count;
int intensity;
int temp;

 


for(int i=0; i<vertiChars; i++){ 
	for(int j=0; j<horizChars; j++){
		sum = 0;
		count = 0;
		// Scan the Chunk
		for( int y = 0; y<scale*verticalScale; y++ ){
			for (int x = 0; x<scale; x++){
				temp = image.at<uchar>(i*scale*verticalScale+y,j*scale+x);
				sum += temp;
				count+=1;
			}
		}
		intensity = sum / count;
		output<<asciiList[intensity/26];
	}
	output<<'\n';
}

output.close();

return 0;

}
